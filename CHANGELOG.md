# Changelog

<!--next-version-placeholder-->
## v1.2.2 (27/04/2023)
### Fixes

- Ignore dest/out directories if they are in input folder

## v1.2.0 (26/04/2023)

### Feature

- Process images and videos recursively

### Changes

- Better build process